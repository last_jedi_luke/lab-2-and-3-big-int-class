//
// Created by Luke on 9/7/2017.
//

#ifndef LAB2_BIG_NUMBER_H
#define LAB2_BIG_NUMBER_H

#include <iostream>
#include <sstream>
#include<string>
#include <vector>
class big_number
{
    int * str_to_int_array;
    int size;

public:

    big_number ();
    big_number(std::string number);
    big_number(int array_value);
    big_number(const big_number &);
    virtual~big_number();
    big_number operator+ (const big_number &);
    big_number operator - (const big_number &);
    big_number operator *(const big_number &);
    big_number &operator =(const big_number &);
    friend std::ostream& operator<< (std::ostream&, const big_number&);
    friend std::istream& operator>> (std::istream&, std::string var);
    int operator %(int j);



};

#endif //LAB2_BIG_NUMBER_H
