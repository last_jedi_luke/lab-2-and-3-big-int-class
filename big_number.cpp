//
// Created by Luke on 9/7/2017.
//
#include "big_number.h"

big_number::big_number(){

    size = 0;
    str_to_int_array = new int[size];

}
big_number::big_number(std::string number)
{
    size = number.length();

    str_to_int_array = new int[size];

    for (int i = 0; i < size; i++)
    {
        str_to_int_array[i] = (number[i]-48);
        //std::cout << str_to_int_array[i];
    }
    std::cout << std::endl;
    // std::cout << size << std::endl;



}

big_number::big_number(const big_number & obj)
{


    size = obj.size;
    str_to_int_array = new int [size];
    std::cout << "Copy Constructor called" << std::endl;


    for (int i=0; i < size ; i++)
    {
        str_to_int_array[i] = obj.str_to_int_array[i];
        std::cout << str_to_int_array[i];

    }

    std::cout << std::endl;



}

big_number::~big_number()
{

    std::cout<< "Destructor called" << std::endl;

    delete [] str_to_int_array;

}


big_number big_number::operator+ (const big_number &rhs){

    big_number temp ("");		//initialize temp variable as an empty string
    int j = 0;
    int i = 0;
    int carry = 0;
    int dif = 0;
    dif = this->size - rhs.size;		//get difference between two array sizes

    if(dif > 0){		//if this is bigger


        temp.size = this->size;
        i = temp.size -1;
        int k = i - dif;

        while ((k) >= 0){

            temp.str_to_int_array[i] = ((this->str_to_int_array[i])+ (rhs.str_to_int_array[k])+carry);
            if (temp.str_to_int_array[i] >= 10 ){
                temp.str_to_int_array[i] = temp.str_to_int_array[i]%10;
                carry = 1;
            }

            else {
                carry = 0;
            }
            i--;
            k--;
        }
        j = dif-1;
        while ( j >= 0){		//once small array is added, take care of the rest of the addition for the bigger array
            temp.str_to_int_array[j] = (this->str_to_int_array[j]+carry);

            if (temp.str_to_int_array[j] >= 10 ){
                temp.str_to_int_array[j] = temp.str_to_int_array[j]%10;
                carry = 1;
            }
            else {
                carry = 0;

            }
            j--;
        }

    }

    else if(dif < 0){		//rhs is bigger
        temp.size = rhs.size;
        i = temp.size -1;
        dif = dif*-1;

        while ((i - dif) >= 0){

            temp.str_to_int_array[i] = ((this->str_to_int_array[i-dif])+ (rhs.str_to_int_array[i])+carry);
            if (temp.str_to_int_array[i] >= 10 ){
                temp.str_to_int_array[i] = temp.str_to_int_array[i]%10;
                carry = 1;
            }

            else {
                carry = 0;
            }
            i--;
        }
        while ( dif >= 0){
            temp.str_to_int_array[dif] = (rhs.str_to_int_array[dif]+carry);

            if (temp.str_to_int_array[dif] >= 10 ){
                temp.str_to_int_array[dif] = temp.str_to_int_array[dif]%10;
                carry = 1;
            }
            else {
                carry = 0;

            }
            dif--;
        }


    }


    else if (dif == 0){		//no difference between array sizes
        temp.size = rhs.size;

        for (i = rhs.size-1; i>0; i--){

            temp.str_to_int_array[i] = ((this->str_to_int_array[i])+ (rhs.str_to_int_array[i])+carry);
            if (temp.str_to_int_array[i] >= 10 ){
                temp.str_to_int_array[i] = temp.str_to_int_array[i]%10;
                carry = 1;
            }

            else {
                carry = 0;
            }
        }

        temp.str_to_int_array[0] = ((this->str_to_int_array[0])+ (rhs.str_to_int_array[0])+carry);
        for (j = 0; j < temp.size; j++){
            std::cout <<temp.str_to_int_array[j];
        }
        std::cout << std::endl;
    }





    return temp;
}


big_number::big_number(int array_value)
{
    int temp = array_value;
    int len = 0;

    while (temp !=0){
        temp = temp/10;
        len++;
    }
    size = len;
    std::cout<<size << std::endl;

    str_to_int_array = new int [size];
    for (int i = size-1; i >= 0; i--){
        str_to_int_array[i] = array_value%10;
        array_value = array_value/10;
    }
    for (int i = 0; i< size; i++){
        std::cout << str_to_int_array[i];
    }

        /*do {
            array_value = array_value/10;
            size++;
        }while (array_value != 0);



    str_to_int_array = new int[size];
    for(int i = size-1; i >= 0; i--) {
        str_to_int_array[i] = temp % 10;
        temp = temp / 10;
    }
*/

std::cout << std::endl;
}

big_number &big_number::operator=(const big_number&obj)
{
    std::cout << "Overload = called" << std::endl;
    if ( this == &obj){
        return *this;
    }
    delete [] str_to_int_array;

    size = obj.size;


        str_to_int_array = new int[size];


        for (int i = 0; i < size; i++){
            str_to_int_array[i] = obj.str_to_int_array[i];
        }




    return *this;


}
std::ostream& operator<< (std::ostream& os, const big_number& obj)
{



    for (int i=0; i<obj.size; i++)
    {
        os << obj.str_to_int_array[i];
    }

    return os;
}

std::istream& operator>> (std::istream& is, std::string var){
    is >> var;
    return is;
}

big_number big_number::operator- (const big_number & rhs) {
    big_number temp("");
    int j = 0;
    int i = 0;
    int k = 1;
    int dif = 0;

    dif = this->size - rhs.size;        //get difference between two array sizes


    if (dif > 0) {//if this is bigger

        temp.size = this->size;
        i = temp.size - 1;

        while ((i - dif) >= 0) {

            temp.str_to_int_array[i] = ((this->str_to_int_array[i]) - (rhs.str_to_int_array[i - dif]));

            if (this->str_to_int_array[i] < rhs.str_to_int_array[i - dif]) {
                this->str_to_int_array[i - 1] -= 1;
                temp.str_to_int_array[i] = (temp.str_to_int_array[i] + 10);

            }

            i--;
        }

        while (dif - k >= 0) {
            temp.str_to_int_array[dif - k] = this->str_to_int_array[dif - k];
            if (this->str_to_int_array[dif - k] < 0) {
                temp.str_to_int_array[dif - k] += 10;
                this->str_to_int_array[dif - k - 1] -= 1;

            }
            k++;
        }

    }
    return temp;
}


    /*else if (dif == 0) {        //no difference between array sizes
        temp.size = rhs.size;

        for (i = rhs.size - 1; i > 0; i--) {

            temp.str_to_int_array[i] = ((this->str_to_int_array[i]) - (rhs.str_to_int_array[i]) + carry);
            if (temp.str_to_int_array[i] >= 10) {
                temp.str_to_int_array[i] = temp.str_to_int_array[i] % 10;
                carry = 1;
            } else {
                carry = 0;
            }
        }

        temp.str_to_int_array[0] = ((this->str_to_int_array[0]) + (rhs.str_to_int_array[0]) + carry);
        for (j = 0; j < temp.size; j++) {
            std::cout << temp.str_to_int_array[j];
        }
        std::cout << std::endl;
    }*/


    /*temp.size = rhs.size;
    //delete [] temp.str_to_int_array;

    int j = 0;
    int i = 0;
    int carry = 0;

    for (i = rhs.size-1; i>0; i--){

    temp.str_to_int_array[i] = ((this->str_to_int_array[i] + carry)- (rhs.str_to_int_array[i]));
    if (this->str_to_int_array[i] < rhs.str_to_int_array[i]){
        temp.str_to_int_array[i] = ((this->str_to_int_array[i] + 10) - (rhs.str_to_int_array[i]));
        carry = -1;
    }

    else {
        carry = 0;
    }
    }

    temp.str_to_int_array[0] = ((this->str_to_int_array[0] + carry)- (rhs.str_to_int_array[0]));
    for (j = 0; j < rhs.size; j++){
        std::cout<<temp.str_to_int_array[j];
    }
std::cout<<std::endl;

    return temp;
}*/


big_number big_number::operator *(const big_number &obj){


        int max = size + obj.size;
        std::vector<int> res(max, 0);
        big_number temp("");
        temp.str_to_int_array = new int[max];

        for (int i = size - 1; i >= 0; i--) {
            int carry = 0;
            int Index = (size - 1) - i;
            int n1 = str_to_int_array[i];

            for (int j = obj.size - 1; j >= 0; j--) {
                int n2 = obj.str_to_int_array[j];
                int sum = n1 * n2 + carry;
                carry = (sum / 10);
                res[Index] += (sum % 10);

                if (res[Index] > 9)
                {
                    res[Index + 1] += (res[Index]/10);
                    res[Index] = (res[Index]%10);
                }
                Index++;
            }
            if (carry > 0) {
                res[Index] += carry;
                if (res[Index] >= 10)
                {
                    res[Index + 1] += (res[Index]/10);
                    res[Index] = (res[Index]%10);
                }
            }
        }
        for (int i = max - 1; i >= 0; i--) {
            if (res[i] != 0) {
                temp.size = i + 1;
                break;
            }
        }

        for (int l = 0, r = temp.size - 1; l < temp.size; l++, r--) {
            temp.str_to_int_array[r] = res[l];
        }

        return temp;
    }


int big_number::operator %(int j){

    int rem = 0;
    int i = 0;
    for( i = 0; i < size; i++){
        rem = ((rem*10 + str_to_int_array[i]) % j);
    }
    return rem;


}




