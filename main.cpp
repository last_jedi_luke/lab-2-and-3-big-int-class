
#include "big_number.h"



//output is below main program
int main() {

    big_number num1 ("10039");
std::string random;
    //std::cin>>random;
   // big_number bob(random);
  //  std::cout<< bob << std::endl;
   // big_number num2 (num1);
    big_number num3 ("19290878361");
    big_number num4 (1234);
    std::cout<< num4 << std::endl;
    //std::cout<< num4 <<std::endl;
    big_number num5 ("11223344");
    big_number num6 ("55667788923");
    big_number num7 ("9871227256");
    big_number num8 ("654387");
   // big_number num9 (123456);
    //std::cout << num9 << std::endl;
   // std::cout << num3 + num1 << std::endl;
    //std::cout << num6 - num5 << std::endl;
    //std::cout << num7 % 21 << std::endl;
    //std::cout << num7*num8 << std::endl;
   // std::cin >> num4;
   // std::cout << num4 << std::endl;
    return 0;
}
/*
Output: So, I experienced many runtime errors when I tried to do all of the different functions (add, subtract, multiply, etc).
I theorize that there is a memory leak somewhere that I have been unable to fix.
Update: Professor Saldamli and I conclude that there is a problem with my compiler. I will try to fix it for future labs.
So I have to run each function one by one in the main.
Subtraction cannot handle negative numbers.
For addition, the first number must be greater than the second (ex. 1234 + 123).
Modulus handles object % int.
Extraction works.
Big_number(int) works.
Copy Constructor works.
Overloaded assignment works.
My friend and I both received help from a friend who took the class last year, so the multiplication code is very similar to someone else's code.
I have been trying to make my add and subtract overloads to not crash but I cannot find the problem.


Addition Output:
 19290888400
Destructor called

 Subtraction Outputs:
 55656565579
Destructor called

 Multiplication Output:
 645959804214
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called

Copy Constructor:
Copy Constructor called
10036
Destructor called
Destructor called

Destructor called
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called

Modulus:
16
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called

Overloaded Extraction:
123456712308
123456712308

123456712308

4
1234

Integer string:
4
1234
1234




Destructor called
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called
Destructor called





*/
